/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiper.pos.imp;

import com.hiper.pos.util.Functions;
import com.suprema.BioMiniSDK;

/**
 *
 * @author emont
 */
public class HuelleroImp {

    BioMiniSDK bioMiniSDK = null;
    Functions functions = new Functions();

    public int count = 0;
    private int nInitFlag = 0;
    private int nCaptureFlag = 0;
    private int nLogListCnt = 0;
    private int nScannerNumber = 0;
    private int nSecurityLevel = 0;
    private int isAutoCapture = 0;
    private int nDetectFake = 2;
    private int nFastMode = 0;
    private int templateType = 0;
    long[] hMatcher = new long[1];

    public HuelleroImp() {
        bioMiniSDK = new BioMiniSDK();
    }

    public int getStatusDevice() {
        int resultCode = bioMiniSDK.UFS_Init();
        System.out.println(functions.getMethodName() + "==>UFS_Init " + resultCode);
        if (resultCode == 0) {
            bioMiniSDK.UFS_SetClassName("demoBioMini");
            resultCode = testCallScanProcCallback();
            if (resultCode == 0) {
                int[] refNumber = new int[1];
                resultCode = bioMiniSDK.UFS_GetScannerNumber(refNumber);
                if (resultCode == 0) {
                    nScannerNumber = refNumber[0];
                    System.out.println(functions.getMethodName() + "==>nScannerNumber " + nScannerNumber);
                    //security level (1~7)
                    int[] refValue = new int[1];
                    if (resultCode == 0) {
                        nSecurityLevel = refValue[0];//
                        System.out.println(functions.getMethodName() + "==>get security level,302(security) value is " + nSecurityLevel);
                    } else {
                        System.out.println(functions.getMethodName() + "==>get security level fail! code: " + resultCode);
                    }
                }
            }
        }
        return resultCode;
    }

    public int testCallScanProcCallback() {
        int nRes = 0;
        // Set the callback function name for getting Scanner Plug event.
        nRes = bioMiniSDK.UFS_SetScannerCallback("demoBioMini");
        System.out.println(functions.getMethodName() + "==>UFS_SetScannerCallback " + nRes);
        return nRes;
    }

    public int connectDevice() {
        // Step 1
        int resultCode = bioMiniSDK.UFS_Init();
        byte[] message = new byte[1024];
        System.out.println(functions.getMethodName() + "==>UFS_Init " + resultCode);
        if (resultCode == 0) {
            System.out.println(functions.getMethodName() + "==>UFS_Init success!!!");
            // Step 2
            bioMiniSDK.UFS_SetClassName("demoBioMini");
            resultCode = testCallScanProcCallback();
            if (resultCode == 0) {
                // Step 3
                int[] refNumber = new int[1];
                resultCode = bioMiniSDK.UFS_GetScannerNumber(refNumber);
                System.out.println(functions.getMethodName() + "==>UFS_GetScannerNumber :" + resultCode);
                if (resultCode == 0) {
                    nScannerNumber = refNumber[0];
                    System.out.println(functions.getMethodName() + "==>nScannerNumber " + nScannerNumber);
                    String licensePath = System.getProperty("user.dir");
                    resultCode = bioMiniSDK.UFM_CreateEx(hMatcher, licensePath);

                    if (resultCode == 0) {

                        //security level (1~7)
                        int[] refValue = new int[1];
                        resultCode = bioMiniSDK.UFM_GetParameter(hMatcher[0], 302, refValue); //302 : security level :UFM_
                        if (resultCode == 0) {
                            nSecurityLevel = refValue[0];//
                            System.out.println(functions.getMethodName() + "==>get security level,302(security) value is  " + nSecurityLevel);
                        } else {
                            System.out.println(functions.getMethodName() + "==>get security level fail! code " + resultCode);
                        }

                        //fast mode
                        int[] refFastMode = new int[1];
                        resultCode = bioMiniSDK.UFM_SetParameter(hMatcher[0], bioMiniSDK.UFM_PARAM_FAST_MODE, refFastMode);
                        if (resultCode == 0) {
                            nFastMode = refFastMode[0];
                            System.out.println(functions.getMethodName() + "==>get fastmode,301(fastmode) value is " + nFastMode);
                            //MsgBox("get fastmode,301(fastmode) value is "+refFastMode.getValue());
                        } else {
                            System.out.println(functions.getMethodName() + "==>get fastmode value fail! code: " + resultCode);
                        }

                        if (hMatcher != null) {
                            switch (templateType) {
                                case 0:
                                    resultCode = bioMiniSDK.UFM_SetTemplateType(hMatcher[0], bioMiniSDK.UFM_TEMPLATE_TYPE_SUPREMA); //2001 Suprema type													
                                    break;
                                case 1:
                                    resultCode = bioMiniSDK.UFM_SetTemplateType(hMatcher[0], bioMiniSDK.UFM_TEMPLATE_TYPE_ISO19794_2); //2002 iso type												
                                    break;
                                case 2:
                                    resultCode = bioMiniSDK.UFM_SetTemplateType(hMatcher[0], bioMiniSDK.UFM_TEMPLATE_TYPE_ANSI378); //2003 ansi type										
                                    break;
                                case 3:
                                    resultCode = bioMiniSDK.UFM_SetTemplateType(hMatcher[0], bioMiniSDK.UFM_TEMPLATE_TYPE_ISO_CARD_COMPACT); //2004 iso card compact
                                    break;
                                case 4:
                                    resultCode = bioMiniSDK.UFM_SetTemplateType(hMatcher[0], bioMiniSDK.UFM_TEMPLATE_TYPE_ISO_CARD_NOMAL); //2005 iso card nomal
                                    break;
                            }
                        }
                        this.getCurrentScannerInfo();
                    } else {
                        System.out.println(functions.getMethodName() + "==>UFM_Create fail!! code : " + resultCode);
                    }
                } else {
                    bioMiniSDK.UFS_GetErrorString(resultCode, message);
                    System.out.println(functions.getMethodName() + "==>GetScannerNumber fail!! :" + new String(message));
                }
            } else {
                bioMiniSDK.UFS_GetErrorString(resultCode, message);
                System.out.println(functions.getMethodName() + "==>UFS_SetScannerCallback fail!! :" + new String(message));
            }
        }
        if (resultCode != 0) {
            bioMiniSDK.UFS_GetErrorString(resultCode, message);
            System.out.println(functions.getMethodName() + "==>Init fail!! return  :" + new String(message));
        }
        return resultCode;
    }

    public int disconnectDevice() {
        int nRes = bioMiniSDK.UFS_Uninit();
        if (nRes == 0) {
            //nRes = bioMiniSDK.UFM_Delete(hMatcher[0]);
            nInitFlag = 0;
            if (nRes == 0) {
                System.out.println(functions.getMethodName() + "==>UFS_Uninit success!!");
            }
        } else {
            System.out.println(functions.getMethodName() + "==>UFS_Uninit fail!!");
        }
        return nRes;
    }

    public int captureImageOnline() {
        int resultCode = 0;
        long[] hScanner = new long[1];
        hScanner = GetCurrentScannerHandle();

        if (hScanner != null) {
            System.out.println("UFS_StartCapturing,get current scanner handle success! : " + hScanner[0]);
        } else {
            System.out.println("UFS_StartCapturing,GetScannerHandle fail!!");
            return 0;
        }

        // Set the callback function name for getting captured frame and the information as second parameter
        resultCode = bioMiniSDK.UFS_StartCapturing(hScanner[0], "captureCallback");

        if (resultCode == 0) {
            System.out.println("UFS_StartCapturing success!!");
            nCaptureFlag = 1;
        } else {
            System.out.println("UFS_StartCapturing fail!! code: ");
        }

        return resultCode;
    }

    public long[] GetCurrentScannerHandle() {
        long[] hScanner = new long[1];
        int nRes = 0;
        int[] nNumber = new int[1];
        nRes = bioMiniSDK.UFS_GetScannerNumber(nNumber);
        if (nRes == 0) {
            if (nNumber[0] <= 0) {
                return null;
            }
        } else {
            return null;
        }

        nRes = bioMiniSDK.UFS_GetScannerHandle(0, hScanner);

        if (nRes == 0 && hScanner != null) {
            return hScanner;
        }
        return null;
    }
    
    /*
     * PARAMETER value
     * for scanner
     * 201 : timeout
     * 202 : brightness
     * 203 : sensitivity
     * 204 : serial 
     * 
     * for extracting
     * 301 : detect core
     * 302 : template size
     * 311 : use sif
     */
    public void getCurrentScannerInfo() {
        long[] hScanner = new long[1];
        int nRes = 0;
        int[] nNumber = new int[1];
        nRes = bioMiniSDK.UFS_GetScannerNumber(nNumber);
        if (nRes == 0) {
            if (nNumber[0] <= 0) {
                return;
            }
        } else {
            return;
        }
        nRes = bioMiniSDK.UFS_GetScannerHandle(0, hScanner);
        if (nRes == 0 && hScanner != null) {
            //getParameter
            int[] pValue = new int[1];
            int nSelectedIdx = 0;
            nRes = bioMiniSDK.UFS_GetParameter(hScanner[0], 201, pValue);
            System.out.println("===>UFS_GetParameter ,201(timeout) value is " + pValue[0]);

            nRes = bioMiniSDK.UFS_GetParameter(hScanner[0], 202, pValue);
            System.out.println("===>UFS_GetParameter ,202(brightness) value is " + pValue[0]);

            nRes = bioMiniSDK.UFS_GetParameter(hScanner[0], 312, pValue);
            System.out.println("===>UFS_GetParameter ,312(detect_fake) value is " + pValue[0]);

            nRes = bioMiniSDK.UFS_GetParameter(hScanner[0], 203, pValue);
            System.out.println("===>UFS_GetParameter ,203(sensitivity) value is " + pValue[0]);
        }
        return;
    }

}
