/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hiper.pos.command;

import com.hiper.pos.imp.HuelleroImp;
import com.hiper.pos.util.Functions;
import com.suprema.BioMiniSDK;

/**
 *
 * @author emont
 */
public class CommandHuellero {

    Functions functions = new Functions();
    HuelleroImp huellero = null;

    public synchronized int obtenerEstado() throws Exception {
        System.out.println("Iniciando " + functions.getMethodName());
        int nRes =0;
        nRes = huellero.getStatusDevice();
        return nRes;
    }

    public synchronized int conectar() throws Exception {
        System.out.println(functions.getMethodName()+" REQUEST ");
        int nRes =0;
        huellero = new HuelleroImp();
        nRes = huellero.connectDevice();
        System.out.println(functions.getMethodName()+" RESPONSE "+nRes);
        return nRes;
    }

    public synchronized int desconectar() throws Exception {
        System.out.println(functions.getMethodName()+" REQUEST ");
        int nRes =0;
        huellero = new HuelleroImp();
        nRes = huellero.disconnectDevice();
        System.out.println(functions.getMethodName()+" RESPONSE "+nRes);
        return nRes;
    }

    public synchronized int capturarImagenOnline() throws Exception {
        System.out.println(functions.getMethodName()+" REQUEST ");
        int nRes =0;
        huellero = new HuelleroImp();
        nRes = huellero.captureImageOnline();
        System.out.println(functions.getMethodName()+" RESPONSE "+nRes);
        return nRes;
    }

    public synchronized int mostrarHuella() throws Exception {
        System.out.println("Iniciando " + functions.getMethodName());
        //this.init();
        //System.out.println("PROCESANDO TEST() ");
        //this.testCallScanProcCallback();
        return 0;//testCallScanProcCallback();
    }

    public synchronized int enrolar() throws Exception {
        System.out.println("Iniciando " + functions.getMethodName());
        //this.init();
        //System.out.println("PROCESANDO TEST() ");
        //this.testCallScanProcCallback();
        return 0;//testCallScanProcCallback();
    }

    public synchronized int verificarHuella() throws Exception {
        System.out.println("Iniciando " + functions.getMethodName());
        //this.init();
        //System.out.println("PROCESANDO TEST() ");
        //this.testCallScanProcCallback();
        return 0;//testCallScanProcCallback();
    }

    public synchronized int identificarHuella() throws Exception {
        System.out.println("Iniciando " + functions.getMethodName());
        //this.init();
        //System.out.println("PROCESANDO TEST() ");
        //this.testCallScanProcCallback();
        return 0;//testCallScanProcCallback();
    }

    public synchronized int guardarHuella() throws Exception {
        System.out.println("Iniciando " + functions.getMethodName());
        //this.init();
        //System.out.println("PROCESANDO TEST() ");
        //this.testCallScanProcCallback();
        return 0;//testCallScanProcCallback();
    }

    
}
